package com.HW_Randomizer;

public class TenRandomNumbersMinusTenToTen {
    public static void main(String[] args) {
        int a = -10, b = 10, counter = 0;
        for (int i = 0; i < 10; i++) {
            counter++;
            int number = a + (int) (Math.random() * (b - a + 1));
            System.out.println("Рандомное число №" + counter + " = " + number);
        }
    }
}
