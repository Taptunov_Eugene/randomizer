package com.HW_Randomizer;

public class TenRandomNumbersZeroToTen {
    public static void main(String[] args) {
        int a = 0, b = 10, counter = 0;
        for (int i = 0; i < 10; i++) {
            counter++;
            int number = a + (int) (Math.random() * (b + 1));
            System.out.println("Рандомное число №" + counter + " = " + number);
        }
    }
}
